(function ($) {
    var DefaultRenderer = {
            people: {
                get: function (data, opts) {
                    var html, x = data;
                    html =  '<div style="margin-bottom: 15px">';
                    html +=     '<div style="width=50px; float: left;">';
                    html +=         '<a href="' + x.url + '">';
                    html +=             '<img src="' + x.image.url + '" width="48" height="48"';
                    html +=             'style="border-radius: 4px" />';
                    html +=         '</a>';
                    html +=     '</div>';
                    html +=     '<div style="margin: 0px 15px; padding: 0px 10px; float: left; width: 418px">';
                    html +=         '<p style="font-size: .9em; margin: 0px 0px">'; 
                    html +=         '<strong>' + x.displayName + '</strong> ' + x.tagline + '</p>';
                    html +=     '</div>';
                    html +=     '<div style="clear: both"></div>';
                    html += '</div>';
                    $(html).appendTo(opts.container);
                }
            },
            activities: {
                list: function (data, opts) {

                },
                get: function (data, opts) {
                    console.log(data);
                    var html, x = data;
                    html =  '<div style="margin-bottom: 15px">';
                    html +=     '<div style="width=50px; float: left;">';
                    html +=         '<a href="' + x.actor.url + '">';
                    html +=             '<img src="' + x.actor.image.url + '" width="48" height="48"';
                    html +=             'style="border-radius: 4px" />';
                    html +=         '</a>';
                    html +=     '</div>';
                    html +=     '<div style="margin: 0px 15px; padding: 0px 10px; float: left; width: 418px">';
                    html +=         '<p style="font-size: .9em; margin: 0px 0px">'; 
                    html +=         '<strong><a href="' + x.actor.url + '">' + x.actor.displayName + '</a></strong>';
                    html +=         ' on ' + new Date(x.published).toDateString() + '<br />';
                    html +=         x.object.content + '</p>';
                    html +=     '</div>';
                    html +=     '<div style="clear: both"></div>';
                    html += '</div>';
                    $(html).appendTo(opts.container);
                }
            },
            comments: {
                list: function (data, opts) {
                    // console.log(data);
                    var i, x, html;
                    for (i = 0; i < data.items.length; i++) {
                        x = data.items[i];
                        html =  '<div style="margin-bottom: 15px">';
                        html +=     '<div style="width=50px; float: left;">';
                        html +=         '<a href="' + x.actor.url + '">';
                        html +=             '<img src="' + x.actor.image.url + '" width="48" height="48"';
                        html +=             'style="border-radius: 4px" />';
                        html +=         '</a>';
                        html +=     '</div>';
                        html +=     '<div style="margin: 0px 15px; padding: 0px 10px; float: left; width: 418px">';
                        html +=         '<p style="font-size: .9em; margin: 0px 0px">'; 
                        html +=         '<strong><a href="' + x.actor.url + '">' + x.actor.displayName + '</a></strong>';
                        html +=         ' on ' + new Date(x.published).toDateString() + '<br />';
                        html +=         x.object.content + '</p>';
                        html +=     '</div>';
                        html +=     '<div style="clear: both"></div>';
                        html += '</div>';
                        $(html).appendTo(opts.container);
                    }
                }
            }
        },
        processData = function (url, opts) {
            $.ajax({url: url, dataType: "json",
                success: function (data) {
                    // console.log(opts);
                    opts.render(data, opts);
                    if (opts.afterRender && typeof opts.afterRender === "function")
                        opts.afterRender();
                }
            });
        },
        baseURI = 'https://www.googleapis.com/plus/v1',
        resources = {
            people: {
                'get': function (opts) {
                    return baseURI + "/people/" + opts.userId + 
                        "?alt=json&maxResults=100&key=" + opts.apiKey;
                },
                'search': function () {},
                'listByActivity': function () {}
            },
            activities: {
                'list': function () { },
                'get': function (opts) {
                    return baseURI + "/activities/" + opts.activityId +
                        "?alt=json&maxResults=100&key=" + opts.apiKey;
                },
                'search': function () {}
            },
            comments: {
                'list': function (opts) {
                    return baseURI + "/activities/" + opts.activityId + "/comments" + 
                        "?alt=json&maxResults=100&key=" + opts.apiKey;
                },
                'get': function () {}
            }
        };

    var sessionOptions = {};

    $.fn.googleplus = function (resource, method, options) {
        var apiUrl,
            defaultOptions = {
                storeData: false,
            };
            options = $.extend({}, defaultOptions, options || {});
            options = $.extend({}, sessionOptions, options || {});
        options.container = this.selector;

        if (resource in resources && method in resources[resource]) {
            apiUrl = resources[resource][method](options);
            if (!options.render)
                options = $.extend(options, {render: DefaultRenderer[resource][method]});
            processData(apiUrl, options);
        } else if (resource === 'plugin') {
            if (method === 'set') {
                sessionOptions = $.extend(sessionOptions, options);
            } else if (method === 'get') {
                return sessionOptions;
            }
        } else {
            $.error('Resource ' + resource + ' with method ' + method + ' does not exist');
        }
    }
})(jQuery);
